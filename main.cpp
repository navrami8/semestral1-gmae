#include "main.hpp"
#include "load.hpp"
#include "graphics.hpp"
#include "data.hpp"

#include "load.cpp"
#include "graphics.cpp"
#include "data.cpp"

using namespace std;

int main() {
    Loader loader;
    loader.LoadConfig();
    loader.LoadMenus();
    loader.LoadOverlay();
    loader.LoadGraphics();
    loader.LoadData();
    Renderer rend = Renderer(loader.getSizeX(),loader.getSizeY());
    vector<Window> windows;
    printf("Please change to full screen \nand change font to Raster 8x8 \nfor the best gameplay experience \npress Enter when ready.");
    getchar();
    for (auto win : rend.Overlay(loader.overlay)){
        windows.push_back(Window(win.second[0],win.second[1],win.second[2],win.second[3]));
    }
    for (int i = 0; i < (int)loader.data_in.size(); i++){
        auto sprt = FindByName(loader.data_in[i].name,loader.sprites);
        auto anim = AnimateSprt(sprt);
        loader.allTiles.push_back(Tile(loader.data_in[i].name,anim,&loader.data_in[i]));
    }
    loader.map=NULL;
    bool loop = true;
    loader.LoadMap("lv1");
    int menu = 0;
    Tile plhol = Tile(*FindByName("won",loader.allTiles));
    int tick = 0;
    bool res = false;
    while(loop){
        if (menu > -1){
            windows[1].Clear();
            windows[0].RenderMenu(loader.menus[menu]);
            windows[1].AddSprite(Coord(0,0),loader.map->player.getCurSprite());
            windows[0].Push(rend);
            windows[1].Push(rend);
            rend.Push();
            if (res){
                rend.Reset();
                res=false;
            }

            if (kbhit()){
                char c = getch();
                if (c == loader.inputs->UP){
                    if (loader.menus[menu].chosen > 0)
                        loader.menus[menu].chosen--;
                }else if(c == loader.inputs->DOWN){
                    if (loader.menus[menu].chosen < (int)loader.menus[menu].OPTIONS.size()-1)
                        loader.menus[menu].chosen++;
                }else if(c == loader.inputs->LEFT){

                }else if(c == loader.inputs->RIGHT){

                }else if(c == loader.inputs->INT1){
                    string vol = loader.menus[menu].OPTIONS[loader.menus[menu].chosen].second;
                    if (!vol.compare("stop"))
                        break;
                    if (!vol.compare("save")){

                    }else if (!vol.compare("load")){

                    }else if (!vol.compare("play")){
                        menu = -1;
                    }else if (!vol.compare("reset")){
                        rend.Reset();
                        system("cls");
                    }else{
                        bool found = false;
                        for(int i = 0; i < (int)loader.menus.size();i++){
                            if (!vol.compare(loader.menus[i].NAME)){
                                menu = i;
                                found = true;
                                break;
                            }
                        }
                        if (!found){
                            loader.LoadMap(vol);
                            menu = -1;
                        }
                    }
                }else if(c == loader.inputs->INT2){
                
                }else if(c == loader.inputs->INTER){
                    loop = false;
                }
            }
        }else{
            windows[1].Clear();
            windows[0].RenderLvl(loader.map);
            windows[1].AddSprite(Coord(0,0),loader.map->inventory->getCurSprite());
            windows[0].Push(rend);
            windows[1].Push(rend);
            rend.Push();

            if (kbhit()){
                char c = getch();
                if (c == loader.inputs->UP){
                    if(loader.map->TestTile(Coord(loader.map->player.x,loader.map->player.y-1)))
                        loader.map->player.y--;
                }else if(c == loader.inputs->DOWN){
                    if(loader.map->TestTile(Coord(loader.map->player.x,loader.map->player.y+1)))
                        loader.map->player.y++;
                }else if(c == loader.inputs->LEFT){
                    if(loader.map->TestTile(Coord(loader.map->player.x-1,loader.map->player.y)))
                        loader.map->player.x--;
                }else if(c == loader.inputs->RIGHT){
                    if(loader.map->TestTile(Coord(loader.map->player.x+1,loader.map->player.y)))
                        loader.map->player.x++;
                }else if(c == loader.inputs->INT1){
                    loader.map->UseItem();
                }else if(c == loader.inputs->INT2){
                    loader.map->Pickup();
                    if(loader.map->inventory->data.returnVal("end").second){
                        windows[1].AddSprite(Coord(0,0),plhol.getCurSprite());
                        windows[1].Push(rend);
                        rend.Push();
                        Sleep(2000);
                        menu = 0;
                        res=true;
                    }
                }else if(c == loader.inputs->INTER){
                    for(int i = 0; i < (int)loader.menus.size();i++){
                        if (!loader.menus[i].NAME.compare("Pause")){
                            menu = i;
                            break;
                        }
                    }
                }
            }
        }
        Sleep(10);
        if (tick==20){
            tick=0;
            loader.map->TickAnims();
            plhol.TickAnim();
        }
        tick++;
    }
}
