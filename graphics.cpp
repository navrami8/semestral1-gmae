#include "main.hpp"
#include "graphics.hpp"

Renderer::Renderer(unsigned short x,unsigned short y): sizeX(x),sizeY(y) {
	shell = vector<Pixel>(x*y,Pixel(' ',White,Black));
	input = vector<Pixel>(x*y,Pixel(' ',White,Black));
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
}

void Renderer::AddPix(const COORD pos,const Pixel &pix){
	input[pos.X+pos.Y*sizeX] = pix;
}

void Renderer::Push() {
	int standard = White+16*Black;
	for(int y = 0; y < sizeY; y++){
		for(int x = 0; x < sizeX; x++){
			Pixel* s = &shell[x+sizeX*y],*i = &input[x+sizeX*y];
			if (*s != *i){
				SetConsoleTextAttribute(hConsole,i->fg+16*i->bg);
				SetXY(x,y);
				putch(i->c);
				shell[x+sizeX*y] = input[x+sizeX*y];
			}
			
		}
	}
	SetConsoleTextAttribute(hConsole,standard);
	SetXY(sizeX,sizeY);
}

void Renderer::SetXY(int x, int y){
	COORD d;
	d.X = x;
	d.Y = y;
	SetXY(d);
}

void Renderer::SetXY(COORD pos){
	SetConsoleCursorPosition(hConsole,pos);
}


void Renderer::AddLine(const COORD start,const COORD end,const Pixel &pix){
	double yy = 1,xx = 1;
	short sx = min((int)end.X,(int)start.X);
	short ex = max((int)end.X,(int)start.X);
	short sy = min((int)end.Y,(int)start.Y);
	short ey = max((int)end.Y,(int)start.Y);
	int lx = ex-sx, ly =ey-sy;
	if (lx!=0)
		yy = (ly*1.0)/lx;
	if (ly!=0)
		xx = (lx*1.0)/ly;
	
	COORD d;
	for(short i = sx; i <= ex; i++){
		d.X = i;
		d.Y = sy+round((i-sx)*yy);
		AddPix(d,pix);
	}
	for(short i = sy; i <= ey; i++){
		d.X = sx+round((i-sy)*xx);
		d.Y = i;
		AddPix(d,pix);
	}
}

void Renderer::AddRectangle(const COORD lt,const COORD rb,const Pixel &pix,const bool fill){
	short minx = min(lt.X,rb.X), miny = min(lt.Y,rb.Y);
	short maxx = max(lt.X,rb.X), maxy = max(lt.Y,rb.Y);
	if (fill){
		for(int x = minx; x <= maxx; x++){
			for(int y = miny; y <= maxy; y++){
				AddPix(Coord(x,y),pix);
			}
		}
	}else{
		AddLine(Coord(minx,miny),Coord(maxx,miny),pix);
		AddLine(Coord(minx,maxy),Coord(maxx,maxy),pix);
		AddLine(Coord(minx,miny),Coord(minx,maxy),pix);
		AddLine(Coord(maxx,miny),Coord(maxx,maxy),pix);
	}
}

void Renderer::AddOutline(const COORD lt,const COORD rb,const Pixel &pix){
	short minx = min(lt.X,rb.X), miny = min(lt.Y,rb.Y);
	short maxx = max(lt.X,rb.X), maxy = max(lt.Y,rb.Y);
	Pixel p = pix;
	p.c='-';
	AddLine(Coord(minx,miny),Coord(maxx,miny),p);
	AddLine(Coord(minx,maxy),Coord(maxx,maxy),p);
	p.c = '|';
	AddLine(Coord(minx,miny),Coord(minx,maxy),p);
	AddLine(Coord(maxx,miny),Coord(maxx,maxy),p);
	p.c = '+';
	AddPix(Coord(minx,miny),p);
	AddPix(Coord(minx,maxy),p);
	AddPix(Coord(maxx,miny),p);
	AddPix(Coord(maxx,maxy),p);
}

void Renderer::AddSprite(const COORD pos,const Sprite *sprite){
	for(int x = 0; x < sprite->size;x++){
		for(int y = 0; y < sprite->size;y++){
			const Pixel *px = &sprite->sprite[x+y*sprite->size];
			if (px->bg != px->fg || px->fg != Black)
				AddPix(Coord(x+pos.X,y+pos.Y),sprite->sprite[x+y*sprite->size]);
		}
	}
}

void Renderer::Fill(const Pixel &pix){
	for(int i = 0; i < (int)input.size();i++){
		input[i] = pix;
	}
}

void Renderer::Reset(){
	for(int i = 0; i < (int)shell.size();i++){
		shell[i] = Pixel(' ',White,Black);
	}
}

void Renderer::Clear(){
	Fill(Pixel(' ',White,Black));
}

void Renderer::AddText(const COORD pos, const Pixel pix,const string text){
	Pixel p = pix;
	COORD d = pos;
	for(int i = 0; i < (int)text.length(); i++){
		p.c=text[i];
		AddPix(d,p);
		d.X++;
	}
}

int ColorCode(string code){
	if (!code.compare("Black"))
		return 0;
	if (!code.compare("Blue"))
		return 1;
	if (!code.compare("Green"))
		return 2;
	if (!code.compare("Cyan"))
		return 3;
	if (!code.compare("Red"))
		return 4;
	if (!code.compare("Purple"))
		return 5;
	if (!code.compare("Brown"))
		return 6;
	if (!code.compare("LightGray"))
		return 7;
	if (!code.compare("Gray"))
		return 8;
	if (!code.compare("LightBlue"))
		return 9;
	if (!code.compare("Lime"))
		return 10;
	if (!code.compare("LightCyan"))
		return 11;
	if (!code.compare("Orange"))
		return 12;
	if (!code.compare("Magenta"))
		return 13;
	if (!code.compare("Yellow"))
		return 14;
	if (!code.compare("White"))
		return 15;
	return 0;
}

vector<pair<char,vector<unsigned char>>> Renderer::Overlay(vector<pair<char,vector<unsigned char>>> overlay){
	vector<pair<char,vector<unsigned char>>> info;
	for( auto op : overlay){
		auto s = op.second;
		switch(op.first){
			case 'f':
				AddRectangle(Coord((int)s[0],(int)s[1]),Coord((int)s[2],(int)s[3]),Pixel((char)s[4],(int)s[5],(int)s[6]),true);
			break;
			case 'o':
				AddOutline(Coord((int)s[0],(int)s[1]),Coord((int)s[2],(int)s[3]),Pixel((char)s[4],(int)s[5],(int)s[6]));
			break;
			case 'r':
				AddRectangle(Coord((int)s[0],(int)s[1]),Coord((int)s[2],(int)s[3]),Pixel((char)s[4],(int)s[5],(int)s[6]),false);
			break;
			case 'w':
			op.second[0] = s[2]-s[0]+1;
			op.second[1] = s[3]-s[1]+1;
			op.second[2] = s[0];
			op.second[3] = s[1];
			info.push_back(op);
			break;
			case 'l':
				AddLine(Coord((int)s[0],(int)s[1]),Coord((int)s[2],(int)s[3]),Pixel((char)s[4],(int)s[5],(int)s[6]));
			break;
			case 't':
				string str = "";
				for (int i = 5; i < (int)s.size();i++)
					str.push_back(s[i]);
				AddText(Coord((int)s[0],(int)s[1]),Pixel((char)s[2],(int)s[3],(int)s[4]),str);
			break;
		}
	}
	return info;
}

void Renderer::Insert(pair<unsigned short,unsigned short> size, pair<unsigned short,unsigned short> offset, vector<Pixel> data){
	for(int i = 0; i < size.second; i++){
		for (int x = 0; x < size.first;x++){
			input[x+offset.first+sizeX*(i+offset.second)] = data[x+size.first*i];
		}
	}
}

Window::Window(unsigned short sx,unsigned short sy,unsigned short ofx,unsigned short ofy)
				:Renderer(sx,sy),offx(ofx),offy(ofy){
		shell = vector<Pixel>();
}

vector<Pixel> Window::GetData() {
	return input;
}

pair<unsigned short,unsigned short> Window::GetSize(){
	return pair<unsigned short,unsigned short>(sizeX,sizeY);
}

pair<unsigned short,unsigned short> Window::GetOffset(){
	return pair<unsigned short,unsigned short>(offx,offy);
}

void Window::RenderMenu(Menu menu){
	Clear();
	int space = (sizeY-menu.OPTIONS.size()-1)/2;
	AddText(Coord((sizeX-menu.NAME.length())/2,++space),Pixel(' ',White,Black),menu.NAME);
	for(int i = 0; i < (int)menu.OPTIONS.size();i++){
		string opt = "";
		if (menu.chosen==i)
			opt.push_back(menu.BRACKETS.first);
		opt.append(menu.OPTIONS[i].first);
		if (menu.chosen==i)
			opt.push_back(menu.BRACKETS.second);
		AddText(Coord((sizeX-opt.length())/2,++space),Pixel(' ',White,Black),opt);
	}
}
void Window::Push(Renderer &rnd){
	rnd.Insert(GetSize(),GetOffset(),GetData());
}

void Window::RenderLvl(Map *map){
	Clear();
	short xp = map->player.x, yp = map->player.y;
	for (int y=0;y<9;y++){
		for (int x=0;x<9;x++){
			short xa=xp-4+x, ya=yp-4+y;
			if (xa<0 || ya <0 || xa/map->sizeX>xp/map->sizeX || ya>=map->sizeY){
				AddSprite(Coord(x*map->empty.sprites[0]->slide->size,y*map->empty.sprites[0]->slide->size),map->empty.sprites[0]->slide);
			}else{
				AddSprite(Coord(x*map->empty.sprites[0]->slide->size,y*map->empty.sprites[0]->slide->size),map->ground[xa+ya*map->sizeX].sprites[0]->slide);
			}
		}
	}
	for(auto itm : map->items){
		short xa=itm.x-xp, ya=itm.y-yp;
		if (abs(xa)<5 && abs(ya)<5){
			AddSprite(Coord((xa+4)*map->empty.sprites[0]->slide->size,(ya+4)*map->empty.sprites[0]->slide->size),itm.getCurSprite());
		}
	}

	for(auto ent : map->entities){
		short xa=ent.x-xp, ya=ent.y-yp;
		if (abs(xa)<5 && abs(ya)<5){
			AddSprite(Coord((xa+4)*map->empty.sprites[0]->slide->size,(ya+4)*map->empty.sprites[0]->slide->size),ent.getCurSprite());
		}
	}
	AddSprite(Coord(4*map->empty.sprites[0]->slide->size,4*map->empty.sprites[0]->slide->size),map->player.getCurSprite());
}