#ifndef GRAPHICS_H

#define GRAPHICS_H

#include "main.hpp"
#include "data.hpp"

using namespace std;

int ColorCode(string code);

struct Renderer{
	protected:
	vector<Pixel> shell;
	vector<Pixel> input;
	HANDLE hConsole;
	void SetXY(COORD pos);
	void SetXY(int x, int y);
	public:
	const unsigned short sizeX, sizeY;
	Renderer(unsigned short x,unsigned short y);
	void Push();
	void Reset();
	void AddPix(const COORD pos,const Pixel &pix);
	void AddSprite(const COORD pos,const Sprite *sprite);
	void AddRectangle(const COORD lt,const COORD rb,const Pixel &pix,const bool fill);
	void AddLine(const COORD start,const COORD length,const Pixel &pix);
	void AddOutline(const COORD lt,const COORD rb,const Pixel &pix);
	void Fill(const Pixel &pix);
	void Clear();
	void AddText(const COORD pos, const Pixel pix,const string text);
	vector<pair<char,vector<unsigned char>>> Overlay(vector<pair<char,vector<unsigned char>>> overlay);
	void Insert(pair<unsigned short,unsigned short> size, pair<unsigned short,unsigned short> offset, vector<Pixel> data);
};

struct Window : Renderer {
	unsigned short offx, offy;
	public:
	Window(unsigned short sx,unsigned short sy,unsigned short ofx,unsigned short ofy);
	vector<Pixel> GetData();
	pair<unsigned short,unsigned short> GetSize();
	pair<unsigned short,unsigned short> GetOffset();
	void RenderMenu(Menu menu);
	void RenderLvl(Map* map);
	void Push(Renderer &rnd);
};

COORD Coord(short x,short y){
	COORD d;
	d.X = x;
	d.Y = y;
	return d;
}

#endif