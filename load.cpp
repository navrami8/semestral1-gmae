#include "main.hpp"
#include "load.hpp"

using namespace std;

Inputs::Inputs(char up = 'w', char down = 's', char left = 'a', char right = 'd', char int1 = ' ', char int2 = (char)9, char inter = (char)27) 
	: UP(up), DOWN(down), LEFT(left), RIGHT(right), INT1(int1), INT2(int2), INTER(inter) {}

Inputs::Inputs() : UP('w'), DOWN('s'), LEFT('a'), RIGHT('d'), INT1(' '), INT2((char)9), INTER((char)27) {}

Settings::Settings(string dir, bool seamless, vector<string> menus) 
	:dir_(dir), SEAMLESS(seamless), MENUS(menus) {}

Settings::Settings() :SEAMLESS(false){}

string Settings::GetDir(){
	return dir_;
}

void Settings::SetDir(string newDir){
	dir_ = newDir;
}

Loader::~Loader(){
	if (inputs != NULL)
		free(inputs);
	if (settings != NULL)
		free(settings);
	if (map != NULL)
		free(map);
}

bool Loader::LoadConfig(){
	ifstream file("config.txt");
	regex regexConfig("(\\w+):(\\w+):(\\w+)");
	char a,w,s,d,int1,int2,inter;
	string dir,temp;
	vector<string> start;
	bool seam = true;
	if (file.is_open())
    {
		string tmp;
        while(getline(file, tmp))
            {
				std::smatch match;
				if(regex_search(tmp, match, regexConfig)){
        			if(!match[1].compare("key")){
						if (!match[3].compare("up")){
							w = SortedInput_(match[2]);
						}else if (!match[3].compare("down")){
							s = SortedInput_(match[2]);
						}else if (!match[3].compare("right")){
							d = SortedInput_(match[2]);
						}else if (!match[3].compare("left")){
							a = SortedInput_(match[2]);
						}else if (!match[3].compare("int1")){
							int1 = SortedInput_(match[2]);
						}else if (!match[3].compare("int2")){
							int2 = SortedInput_(match[2]);
						}else if (!match[3].compare("interrupt")){
							inter = SortedInput_(match[2]);
						}
        			}
					else if(!match[1].compare("set"))
					{
						if (!match[3].compare("menu")){
							temp = match[2];
							start.push_back(temp+".txt");
						}else if (!match[3].compare("seamless")){
							if (!match[2].compare("false"))
									seam = false;
						}else if (!match[3].compare("dir")){
							dir = match[2];
						}
					}
        			else
					{
            			return false;
        			}
    			}
			}
        file.close();
    }
    else
    {
        return false;
    }
	inputs = new Inputs(w,s,a,d,int1,int2,inter);
	settings = new Settings(dir,seam,start);
	return true;
}

char Loader::SortedInput_(string s){
	if (s.length() == 1){
		return s[0];
	}
	if (!s.compare("tab")){
		return '\t';
	}
	if (!s.compare("esc")){
		return '\033';
	}
	if (!s.compare("space")){
		return ' ';
	}
	return '\0';
}

bool Loader::LoadMenus(){
	string path = "./"+ settings->GetDir() + "/Common";
	for(int i = 0; i < (int)settings->MENUS.size();i++){
		for (const auto & entry : filesystem::directory_iterator(path)){
			if (!entry.path().filename().compare(settings->MENUS[i])){
				pair<char,char> br;
				pair<string,string> opt;
				vector<pair<string,string>> opts;
				ifstream file(entry.path());
				string tmp,name;
				getline(file,tmp);
				br.first=tmp[0];
				br.second=tmp[1];
				tmp.erase(0,3);
				name = tmp;
				while(getline(file,tmp)){
					int x = tmp.find(':');
					opt.first = tmp.substr(0,x);
					tmp.erase(0,x+1);
					opt.second = tmp;
					opts.push_back(opt);
				}
				menus.push_back(Menu(br,name,opts));
			}
		}
	}
	return true;
}

short Loader::getSizeX(){
	return  sizeX;
}

short Loader::getSizeY(){
	return  sizeY;
}

bool Loader::LoadOverlay(){
	string path = "./"+ settings->GetDir() + "/Common/overlay.txt";
	ifstream file(path);
	string tmp,ops,tmp2;
	pair<char,vector<unsigned char>> com;
	vector<unsigned char> props;
	char op;
	getline(file,tmp);
	int x = tmp.find(':');
	sizeX = stoi(tmp.substr(0,x));
	tmp.erase(0,x+1);
	sizeY = stoi(tmp);
	while(getline(file,tmp))
	{
		int x = tmp.find(':');
		ops = tmp.substr(0,x);
		tmp.erase(0,x+1);
		if(!ops.compare("fill")){
			op = 'f';
		}else if(!ops.compare("outline")){
			op = 'o';
		}else if(!ops.compare("rectangle")){
			op = 'r';
		}else if(!ops.compare("window")){
			op = 'w';
		}else if(!ops.compare("line")){
			op = 'l';
		}else if(!ops.compare("text")){
			op = 't';
		}
		while (op!='t'){
			x = tmp.find(':');
			if (x == (int)tmp.npos)
				break;
			tmp2 = tmp.substr(0,x);
			tmp.erase(0,x+1);
			try
			{
				x = stoi(tmp2);
			}
			catch (invalid_argument &e)
			{
				if (tmp2.length() == 1){
					x = (int)tmp2[0];
				}
				else{
					x = ColorCode(tmp2);
				}
			}
		props.push_back(x);
		}
		if (op == 't'){
			for (int i = 0; i < 5;i++){
				x = tmp.find(':');
				if (x == (int)tmp.npos)
					break;
				tmp2 = tmp.substr(0,x);
				tmp.erase(0,x+1);
				try
				{
					x = stoi(tmp2);
				}
				catch (invalid_argument &e)
				{
					if (tmp2.length() == 1){
						x = (int)tmp2[0];
					}
					else{
						x = ColorCode(tmp2);
					}
				}
				props.push_back(x);
			}
			for (int i = 0; i < (int)tmp.length();i++){
				props.push_back(tmp[i]);
			}
			props.back() = '\0';
		}
		com.first=op;
		com.second = props;
		overlay.push_back(com);
		props.clear();
	}
	return true;
}

bool Loader::LoadGraphics(){
	string path = "./"+ settings->GetDir() + "/Graphics";
		for (const auto & entry : filesystem::directory_iterator(path)){
			if (!entry.path().extension().compare(".pn")){
				ifstream file(entry.path());
				string name = entry.path().filename().generic_string();
				name.pop_back();
				name.pop_back();
				name.pop_back();
				string tmp;
				string phase;
				short num;
				vector<Pixel> pixels;
				while (getline(file,tmp)){
					int x = tmp.find(':');
					phase = tmp.substr(0,x);
					tmp.erase(0,x+1);
					num = stoi(tmp);
					int i = 0;
					do{
						getline(file,tmp);
						for(int y = 0;y<(int)tmp.length();y+=3){
							pixels.push_back(Pixel(tmp[y],tmp[y+1]-'a',tmp[y+2]-'a'));
						}
						i++;
					}while(i<(int)tmp.length()/3);
					sprites.push_back(Sprite(name,phase,num,i,pixels));
					pixels.clear();
				}
				
			}
		}
		return true;
}

bool Loader::LoadData(){
	string path = "./"+ settings->GetDir() + "/Metadata";
	data_in.clear();
		for (const auto & entry : filesystem::directory_iterator(path)){
			if (!entry.path().extension().compare(".dt")){
				ifstream file(entry.path());
				vector<pair<string,int>> data;
				string name = entry.path().filename().generic_string();
				name.pop_back();
				name.pop_back();
				name.pop_back();
				string tmp;
				getline(file,tmp);
				int type =-1;
				if (!tmp.compare("tile"))
					type = 0;
				else if (!tmp.compare("entity"))
					type = 1;
				else if (!tmp.compare("item"))
					type = 2;
				pair<string,int> pair;
				while (getline(file,tmp)){
					int x = tmp.find(':');
					pair.first = tmp.substr(0,x);
					tmp.erase(0,x+1);
					pair.second = stoi(tmp);
					data.push_back(pair);
				}
				data_in.push_back(Metadata(name,data,type));
			}
		}
	return true;
}

bool Loader::LoadMap(string name){
	string path = "./"+ settings->GetDir() + "/Metadata";
	if (map != NULL)
		free(map);
	for (const auto & entry : filesystem::directory_iterator(path)){
		if (!entry.path().extension().compare(".mp")){
			ifstream file(entry.path());
			vector<pair<string,int>> data;
			string nm = entry.path().filename().generic_string();
			nm.pop_back();
			nm.pop_back();
			nm.pop_back();
			if (!name.compare(nm)){
				string tmp;
				vector<Tile> ground;
				vector<pair<string,char>> code;
				pair<string,char> temp;
				vector<Entity> ent;
				vector<Item> items;
				Entity *player;
				Tile *mt;
				int x = 0, y = 0;
				getline(file,tmp);
				do{
					temp.second = tmp[0];
					tmp.erase(0,2);
					temp.first = tmp;
					code.push_back(temp);
					getline(file,tmp);
				}while(tmp.compare("//"));
				while(true){
					getline(file,tmp);
					if(!tmp.compare("//"))
						break;
					y++;
					x = tmp.length();
					for (char a : tmp){
						for(int p = 0; p < (int)code.size();p++){
							pair<std::string, char> *plate = &code[p];
							if (a == plate->second){
								for (int l = 0; l < (int)allTiles.size();l++){
									Tile *e = &allTiles[l];
									if (!plate->first.compare(e->name)){
										if (e->data->type == 0){
											ground.push_back(Tile(*e));
											break;
										}
									}
								}
								break;
							}
						}
					}
				}
				while(getline(file,tmp)){
					string des,tmp1;
					int i,xi,yi;
					i = tmp.find(':');
					des = tmp.substr(0,i);
					tmp.erase(0,i+1);
					i = tmp.find(':');
					xi = stoi(tmp.substr(0,i));
					tmp.erase(0,i+1);
					yi = stoi(tmp);
					for(auto t : allTiles){
						if (!t.name.compare("empty"))
							mt=new Tile(t);
						if (!t.name.compare(des)){
							if (t.data->type == 1){
								if (des.compare("player"))
									ent.push_back(Entity(t,xi,yi));
								else
									player = new Entity(t,xi,yi);
							}else if (t.data->type == 2){
								items.push_back(Item(t,xi,yi));
							}
							break;
						}
					}
				}
				map = new Map(nm,x,y,ground,ent,items,*player,*mt);
				free(player);
				free(mt);
				return true;
			}
		}
	}
	return false;
}
