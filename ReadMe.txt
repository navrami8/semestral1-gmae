Hra:
	Ovládání: 	awsd - pohyb
			mezerník - použij/vyber
			e - seber/vyměň
			Esc - pauza/stop;
			           -----
	úkolem je sebrat mince    /     \
				  | XXX |
				  | X   |
				  | X   |
				  | X   |
				  | XXX |
				  \     /
			           -----
Úpravy:
	ovládání:
		možné v config.txt
	levely:
		možné v Data-metadata
		koncovky .lv
	objekty:
		možné v Data-metadata a Data-graphics
		koncovky .dt a .pn
	menu:
		možné v Data-common
		menu_.txt
	pozadí:
		možné v Data-common
		overlay.txt
