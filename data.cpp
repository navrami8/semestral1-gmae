#include "main.hpp"
#include "data.hpp"

using namespace std;


Pixel::Pixel(char ch,unsigned int foreground , unsigned int background):c(ch),fg(foreground),bg(background){}

Pixel::Pixel(const Pixel &that){
	this->bg = that.bg;
	this->c = that.c;
	this->fg = that.fg;
}

Pixel& Pixel::operator=(const Pixel &A ){
	this->bg = A.bg;
	this->c = A.c;
	this->fg = A.fg;
	return *this;
}

bool Pixel::operator==(const Pixel &A ){
	return this->c == A.c && this->fg == A.fg && this->bg == A.bg;
}
bool Pixel::operator!=(const Pixel &A ){
	return (this->c != A.c || this->fg != A.fg || this->bg != A.bg);
}

Sprite::Sprite(string nm, string stat, short number, short wh, vector<Pixel> colors)
				:name(nm), state(stat), num(number), size(wh), sprite(colors){}

Anim::Anim(Anim *prev_,Sprite *sprite,Anim *next_):prev(prev_),next(next_),slide(sprite){}
Anim::Anim(Anim *prev_,Sprite *sprite):Anim(prev_,sprite,NULL){}
Anim::Anim(Sprite *sprite):Anim(NULL,sprite,NULL){}

Menu::Menu(const pair<char,char> brackets,const string name, const vector<pair<string,string>> options):
			BRACKETS(brackets),NAME(name), OPTIONS(options), chosen(0){}

Metadata::Metadata(const string nam,const vector<pair<string,int>> dat, const unsigned short typ) :name(nam),data(dat), type(typ){}

Metadata::Metadata(const Metadata &dat) :name(dat.name),data(dat.data),type(dat.type){}

pair<string,int> Metadata::returnVal(const string &name){
	for (auto a : data){
		if (!a.first.compare(name))
			return a;
	}
	return pair("nil",0);
}

Tile::Tile(const Tile &tile):name(tile.name),sprites(tile.sprites),data(tile.data){}

Tile::Tile(string name, vector<Anim*> &all_sprites,Metadata *dat)
				:name(name),data(dat){
					for (int i =0; i < (int)all_sprites.size();i++){
						auto sprite = all_sprites[i];
						sprites.push_back(new Anim(sprite->slide));
						auto x = sprite->next;
						auto a = sprites[i];
						while (x!=sprite)
						{
							a->next=new Anim(a,x->slide);
							x=x->next;
							a=a->next;
						}
						a->next=sprites[i];
						sprites[i]->prev=a;
					}
				}

Tile::~Tile(){
	/*
	Anim *o,*n,*p;
	for(int i=0; i < (int)sprites.size();i++){
		p=sprites[i];
		o=p->next;
		while(o!=p && o!=NULL && o!=((Anim*)(0xabababab))){
			n=o->next;
			free(o);
			o=n;
		}
		free(o);
	}
	*/
}

void Tile::TickAnim(){
	sprites[0]=sprites[0]->next;
}

Sprite* Tile::getCurSprite(){
	return (sprites[0]->slide);
}

Entity::Entity(const Tile &tile, short posX, int posY):Tile(tile),data(*tile.data),x(posX),y(posY){
	state = "idle";
	auto d = data.returnVal("idlea");
	play_once = 0;
	if (d.first.compare("nil"))
		play_once = d.second<1;
}
void Entity::TickAnim(){
	int i = 0;
	while (sprites[i]->slide->state!=state)
		i++;
	sprites[i]=sprites[i]->next;
	if (sprites[i]->slide->num==1 && play_once){
		play_once = false;
		if(state.compare("dying"))
			SwState("idle");
		else
			SwState("dead");
	}
}

void Entity::SwState(string newstate){
	int i = 0;
	while (sprites[i]->slide->state!=state)
		i++;
	while(sprites[i]->slide->num!=1)
		sprites[i]=sprites[i]->next;
	state=newstate;
}

Sprite* Entity::getCurSprite(){
	int i = 0;
	while (sprites[i]->slide->state!=state)
		i++;
	return (sprites[i]->slide);
}

vector<Sprite*> FindByName(string name,vector<Sprite> &all_sprites){
	vector<Sprite*> filtered;
	for (int i = 0; i < (int)all_sprites.size();i++){
		if (!all_sprites[i].name.compare(name))
			filtered.push_back(&all_sprites[i]);
	}
	return filtered;
}

Metadata* FindByName(string name,vector<Metadata> &dat){
	for (int i = 0; i < (int)dat.size();i++){
		if (!dat[i].name.compare(name))
			return &dat[i];
	}
	return NULL;
}

Tile* FindByName(string name,vector<Tile> &all_tiles){
	for (int i = 0; i < (int)all_tiles.size();i++){
		if (!all_tiles[i].name.compare(name))
			return &all_tiles[i];
	}
	return NULL;
}

vector<Entity*> FindByName(string name,vector<Entity> &all_ent){
	vector<Entity*> filtered;
	for (int i = 0; i < (int)all_ent.size();i++){
		if (!all_ent[i].name.compare(name))
			filtered.push_back(&all_ent[i]);
	}
	return filtered;
}

vector<Anim*> AnimateSprt(vector<Sprite*> sprites){
	vector<Anim*> out;
	short test;
		for(int i = 0;i<(int)sprites.size();i++){
			test = 1;
			for(int y=0;y<(int)out.size();y++){
				auto pfff = sprites[i]->state;
				if(out[y]->slide->state==sprites[i]->state){
					test=0;
					Anim *x = out[y];
					while (x->next!=NULL)
						x=x->next;
					x->next=new Anim(x,sprites[i],NULL);
					break;
				}
			}
			if (test) {
				out.push_back(new Anim(NULL,sprites[i],NULL));
			}
		}
		for(int y=0;y<(int)out.size();y++){
				Anim *x = out[y];
				while (x->next!=NULL)
						x=x->next;
				out[y]->prev=x;
				x->next=out[y];
			}
	return out;
}

Item::Item(const Tile &tile, short posX, int posY)
	:Tile(tile),data(*tile.data),x(posX),y(posY)
	{}

Map::Map(const string &name,const int sX ,const int sY,const  vector<Tile> &grnd,const  vector<Entity> &enms,const  vector<Item> &itms,const  Entity &pl,const Tile &emp)
	:name(name),sizeX(sX),sizeY(sY),ground(grnd),empty(emp),entities(enms),items(itms),inventory(new Item(emp,0,0)),player(pl){}

Map::Map(const Map &that)
	:name(that.name),sizeX(that.sizeX),sizeY(that.sizeY),ground(that.ground),empty(that.empty),entities(that.entities),items(that.items),inventory(new Item(that.empty,0,0)),player(that.player){}

	
bool Map::TestTile(COORD coord){
	if (coord.X<0 || coord.Y<0 || coord.X>sizeX-1 ||coord.Y>sizeY-1)
		return false;
	int pos = coord.X+coord.Y*sizeX;
	auto x = ground[pos].data->returnVal("walk");
	for(int i=0; i<(int)entities.size();i++){
		if (coord.X==entities[i].x && coord.Y==entities[i].y)
			return false;
	}
	return x.second;
}

void Map::TickAnims(){
	for (int i = 0; i < (int)ground.size(); i++){
		ground[i].sprites[0] = ground[i].sprites[0]->next;
	}
	for (int i = 0; i < (int)entities.size(); i++){
		entities[i].TickAnim();
	}
	for (int i = 0; i < (int)items.size(); i++){
		items[i].TickAnim();
	}
	player.TickAnim();
	inventory->TickAnim();
	CheckLiving();
}

void Map::Pickup(){
	for (int i=0;i<(int)items.size();i++){
		if(items[i].x==player.x && items[i].y==player.y){
			if (inventory->name.compare("empty")){
				inventory->x=player.x;
				inventory->y=player.y;
				items.push_back(*inventory);
			}
			free(inventory);
			inventory = new Item(items[i]);
			RemoveItm(i);
			break;
		}
	}
}

void Map::RemoveItm(int posInVec){
	string tnm = items[posInVec].name;
	int tx = items[posInVec].x, ty = items[posInVec].y;
	vector<Item> newit;
	for(int i = 0; i < (int)items.size();i++){
		Item t = Item(items[i]);
		if (!((!t.name.compare(tnm)) && t.x==tx && t.y==ty))
			newit.push_back(t);
	}
	items.clear();
	items.swap(newit);
}

void Map::RemoveEnt(int posInVec){
	string tnm = entities[posInVec].name;
	int tx = entities[posInVec].x, ty = entities[posInVec].y;
	vector<Entity> newit;
	for(int i = 0; i < (int)entities.size();i++){
		Entity t = Entity(entities[i]);
		if (!((!t.name.compare(tnm)) && t.x==tx && t.y==ty))
			newit.push_back(t);
	}
	entities.clear();
	entities.swap(newit);
}

void Map::UseItem(){
	if(inventory->name.compare("empty")){
        if(inventory->data.returnVal("use").second){
			for(int i=0;i<(int)entities.size();i++){
				int xd = abs(entities[i].x-player.x), yd=abs(entities[i].y-player.y);
				if((xd==1)!=(yd==1) && xd<2 && yd <2){
					if(entities[i].data.returnVal("boom").second && inventory->data.returnVal("boom").second){
						entities[i].SwState("dying");
						entities[i].play_once=true;
						free(inventory);
						inventory = new Item (empty,0,0);
					}
				}
			}
        }
    }
}

void Map::CheckLiving(){
	for(int i=0;i<(int)entities.size();i++){
		if(!entities[i].state.compare("dead")){
			RemoveEnt(i);
		}
	}
}
