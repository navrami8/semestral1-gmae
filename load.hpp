#ifndef LOAD_H
#define LOAD_H

#include "main.hpp"
#include "graphics.hpp"

using namespace std;

struct Inputs{
	public:
	const char UP;
	const char DOWN;
	const char LEFT;
	const char RIGHT;
	const char INT1;
	const char INT2;
	const char INTER;
	Inputs(char up, char down, char left, char right, char int1, char int2, char inter);
	Inputs();
};

struct Settings{
	string dir_;
	public:
	const bool SEAMLESS;
	const vector<string> MENUS;
	Settings();
	Settings(string dir, bool seamless, vector<string> menus);
	string GetDir();
	void SetDir(string newDir);
};

struct Loader {
	char SortedInput_(string s );
	short sizeX,sizeY;
	public:
	Inputs *inputs;
	Settings *settings;
	Map *map;
	vector<Sprite> sprites;
	vector<Menu> menus;
	vector<pair<char,vector<unsigned char>>> overlay;
	vector<Metadata> data_in;
	vector<Tile> allTiles;
	short getSizeX();
	short getSizeY();
	bool LoadConfig();
	bool LoadOverlay();
	bool LoadGraphics();
	bool LoadMenus();
	bool LoadData();
	bool LoadMap(string name);
	~Loader();
};

#endif