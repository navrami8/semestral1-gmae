#ifndef DATA_H

#define DATA_H

#include "main.hpp"

using namespace std;

 enum Colors{
	Black = 0,
	Blue = 1,
	Green = 2,
	Cyan = 3,
	Red = 4,
	Purple = 5,
	Brown = 6,
	LightGray = 7,
	Gray = 8,
	LightBlue = 9,
	Lime = 10,
	LightCyan = 11,
	Orange = 12,
	Magenta = 13,
	Yellow = 14,
	White = 15
};

struct Pixel{
	public:
	unsigned char c;
	unsigned int fg :4;
	unsigned int bg :4;
	Pixel(char c,unsigned int fg , unsigned int bg);
	Pixel(const Pixel &that);
	Pixel& operator=(const Pixel &A );
	bool operator==(const Pixel &A );
	bool operator!=(const Pixel &A );
};

struct Sprite{
	public:
	const string name;
	const string state;
	const short num;
	const short size;
	const vector<Pixel> sprite;
	Sprite(string nm, string stat, short number, short wh, vector<Pixel> colors);
};

struct Anim {
	public:
	Anim *prev, *next;
	Sprite *slide;
	Anim(Anim *prev_,Sprite *sprite,Anim *next_);
	Anim(Anim *prev_,Sprite *sprite);
	Anim(Sprite *sprite);
};

struct Menu{
	public:
	const pair<char,char> BRACKETS;
	const string NAME;
	const vector<pair<string,string>> OPTIONS;
	int chosen;
	Menu(const pair<char,char> brackets,const string name, const vector<pair<string,string>> options);
};

struct Metadata{
	public:
	const string name;
	const vector<pair<string,int>> data;
	const unsigned short type;
	Metadata(const string nam,const vector<pair<string,int>> dat, const unsigned short typ);
	Metadata(const Metadata &dat);
	pair<string,int> returnVal(const string &name);
};

struct Tile {
	public:
	const string name;
	vector<Anim*> sprites;
	Metadata *data;
	Tile(string name, vector<Anim*> &all_sprites,Metadata *dat);
	Tile(const Tile &tile);
	~Tile();
	void TickAnim();
	Sprite* getCurSprite();
};

struct Entity : Tile{
	public:
	Metadata data;
	short x,y;
	bool play_once;
	string state;
	Entity(const Tile &tile, short posX, int posY);
	void TickAnim();
	void SwState(string newstate);
	Sprite* getCurSprite();
};

struct Item : Tile{
	public:
	Metadata data;
	short x,y;
	Item(const Tile &tile, short posX, int posY);
};

struct Map{
	public:
	const string name;
	const int sizeX,sizeY;
	vector<Tile> ground;
	const Tile empty;
	vector<Entity> entities;
	vector<Item> items;
	Item *inventory;
	Entity player;
	Map(const string &name,const int sX ,const int sY, const  vector<Tile> &grnd,const  vector<Entity> &enms,const  vector<Item> &itms,const  Entity &pl, const Tile &emp);
	Map(const Map &that);
	bool TestTile(COORD coord);
	void TickAnims();
	void Pickup();
	void RemoveItm(int posInVec);
	void RemoveEnt(int posInVec);
	void UseItem();
	void CheckLiving();
};


vector<Sprite*> FindByName(string name,vector<Sprite> &all_sprites);
Metadata* FindByName(string name,vector<Metadata> &dat);
#endif